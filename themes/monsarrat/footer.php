
			<footer id="main-footer">
				<div class="container clearfix et_menu_container footer-nav-wrap">
				<div class="abcd">
					
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo-footer.png" alt="Monsarrat" id="logo-footer">
					</a>

				</div>
				<div id="" class="dddd" data-height="66" data-fixed-height="40">
					<nav id="">
						<ul id="" class="nav">

							<?php
	                            /**
	                             * Displays a navigation menu
	                             * @param array $args Arguments
	                             */
	                            $args = array(
	                            	'theme_location' => '',
	                            	'menu' => 'Page menu',
	                            	'menu_class' => 'nav navbar-nav navbar-right nav-footer',
	                            	'menu_id' => '',
	                            	'echo' => true,
	                            	'fallback_cb' => 'wp_page_menu',
	                            	'before' => '',
	                            	'after' => '',
	                            	'link_before' => '',
	                            	'link_after' => '',
	                            	'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
	                            	'depth' => 0,
	                            	'walker' => ''
	                            	);

	                            wp_nav_menu($args);

	                            ?>
	                        </ul>
	                    </nav>
		            </div> <!-- #et-top-navigation -->
		            
		        </div> <!-- .container -->
		        <div class="">
	            	<p class="mrs-copy-right">Copyright © 2016 Monsarrat, Inc. </p>
	            </div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->
	</div> <!-- #page-container -->
	<?php wp_footer(); ?>
</body>
</html>