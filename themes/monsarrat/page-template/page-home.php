<?php 

get_header();

?>
<div id="main-content">



	<article id="post-42" class="post-42 page type-page status-publish hentry">


		<div class="entry-content">
			<div class="et_pb_section et_pb_fullwidth_section  et_pb_section_0 et_section_regular">
				
				

				<div class="et_pb_module et-waypoint et_pb_fullwidth_image et_pb_animation_fade_in  
et_pb_fullwidth_image_0">
				<img src="<?php echo get_template_directory_uri(); ?>/images/Hero-Image.png" alt="" />

				</div>
				
			</div> <!-- .et_pb_section --><div class="et_pb_section  et_pb_section_1 et_section_regular">



			<div class=" et_pb_row et_pb_row_0">
				
				<div class="et_pb_column et_pb_column_1_2  et_pb_column_0">

					<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_0 
et_always_center_on_mobile">
					<img src="<?php echo get_template_directory_uri(); ?>/images/Bitmap.png" alt="" />

					</div>
				</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_1">
				
				<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  
et_pb_text_0">

					<p>Forget online gaming</p>

				</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_space et_pb_divider 
et_pb_divider_0 et-hide-mobile"></div><div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  
et_pb_text_1">
				
				<p>The new Gaming isn’t an alienating, separating break from life. It is life. It is 
posibly. It is friendship. It is the serendipity of millions of people, around the world, who want to engage.</p>

			</div> <!-- .et_pb_text --><div class="et_pb_text et_pb_module et_pb_bg_layout_light 
et_pb_text_align_left  et_pb_text_2">

			<p>Pokemon GO was just the start. New technology from Monsarrat, Inc. and partnes takes the best 
concepts from the $20 billion Masively Multiplayer Online game market and applies them to the real-world, map-driven, 
graphics rich technology called Augmented Reality. We don’t just create a magic world where anything can happen. We place 
that world into daily life, all around you.</p>

		</div> <!-- .et_pb_text --><div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  
et_pb_text_3">

		<p>This is Massively Real World Gaming</p>

	</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section --><div class="et_pb_section  et_pb_section_2 et_pb_with_background et_section_regular">



<div class=" et_pb_row et_pb_row_1">

	<div class="et_pb_column et_pb_column_4_4  et_pb_column_2">

		<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_text_4">

			<p>Awesome Video</p>

		</div> <!-- .et_pb_text -->
	</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_2">

<div class="et_pb_column et_pb_column_1_3  et_pb_column_3 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_4">

<div class="et_pb_module et_pb_space et_pb_divider et_pb_divider_1"></div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_5 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_3">

<div class="et_pb_column et_pb_column_1_4  et_pb_column_6 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_7">

<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_text_5">

	<p>The new Gaming isn’t an alienating, separating break from life. It is life. It is posibly. It is friendship. It 
is the serendipity of millions of people, around the world, who want to engage.</p>

</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_8 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_4">

<div class="et_pb_column et_pb_column_4_4  et_pb_column_9">

	<div class="et_pb_module et_pb_video  et_pb_video_0">
		<div class="et_pb_video_box">
			<iframe width="1080" height="608" src="https://www.youtube.com/embed/7poGT0d8Eeo?feature=oembed" 
frameborder="0" allowfullscreen></iframe>
		</div>
		<div class="et_pb_video_overlay" style="background-image: url(wp-content/uploads/2016/12/testimonials-
boston-common.jpg);">
			<div class="et_pb_video_overlay_hover">
				<a href="#" class="et_pb_video_play"></a>
			</div>
		</div>
	</div>
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section --><div class="et_pb_section  et_pb_section_3 et_section_regular">



<div class=" et_pb_row et_pb_row_5">

	<div class="et_pb_column et_pb_column_1_4  et_pb_column_10">

		<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_0 
et_pb_blurb_position_top">
			<div class="et_pb_blurb_content">
			<div class="et_pb_main_blurb_image"><a href="#" target="_blank"><img src="<?php echo 
get_template_directory_uri(); ?>/images/Seek-It.png" alt="seek it" class="et-waypoint et_pb_animation_top" /></a></div>
				<div class="et_pb_blurb_container">
					<h4><a href="#" target="_blank">Seek it</a></h4>


				</div>
			</div> <!-- .et_pb_blurb_content -->
		</div> <!-- .et_pb_blurb -->
	</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_11">

	<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_1 
et_pb_blurb_position_top">
		<div class="et_pb_blurb_content">
		<div class="et_pb_main_blurb_image"><a href="#" target="_blank"><img src="<?php echo 
get_template_directory_uri(); ?>/images/Make-it.png" alt="make it" class="et-waypoint et_pb_animation_top" /></a></div>
			<div class="et_pb_blurb_container">
				<h4><a href="#" target="_blank">Tweak it</a></h4>

			</div>
		</div> <!-- .et_pb_blurb_content -->
	</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_12">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_2 
et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); 
?>/images/Fight-It.png" alt="Fight it" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="#" target="_blank">Fight it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_13">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_3 
et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); 
?>/images/Greet.png" alt="" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="#" target="_blank">Greet it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_6">

<div class="et_pb_column et_pb_column_1_4  et_pb_column_14">

	<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_4 
et_pb_blurb_position_top">
		<div class="et_pb_blurb_content">
		<div class="et_pb_main_blurb_image"><a href="#" target="_blank"><img src="<?php echo 
get_template_directory_uri(); ?>/images/Page-1.png" alt="Dare it" class="et-waypoint et_pb_animation_top" /></a></div>
			<div class="et_pb_blurb_container">
				<h4><a href="#" target="_blank">Dare it</a></h4>


			</div>
		</div> <!-- .et_pb_blurb_content -->
	</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_15">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_5 
et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); 
?>/images/Share-It.png" alt="Share it" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="#" target="_blank">Share it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_16">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_6 
et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); 
?>/images/Join-It.png" alt="Join it" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="#" target="_blank">Join it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_17">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_7 
et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="#" target="_blank"><img src="<?php echo get_template_directory_uri(); 
?>/images/Beat.png" alt="Beat it" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="#" target="_blank">Beat it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section --><div class="et_pb_section  et_pb_section_4 et_pb_with_background et_section_regular">



<div class=" et_pb_row et_pb_row_7">

	<div class="et_pb_column et_pb_column_4_4  et_pb_column_18">

		<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_6">

			<p>Get it for free!</p>

		</div> <!-- .et_pb_text -->
	</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_8">

<div class="et_pb_column et_pb_column_1_3  et_pb_column_19 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_20">

<div class="et_pb_module et_pb_space et_pb_divider et_pb_divider_2"></div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_21 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_9">

<div class="et_pb_column et_pb_column_1_4  et_pb_column_22 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_23">

<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_7">

	<p>Get free “The History and Founding of the $20 Billion MMO Industry” when you subscribe for occasional news from 
Montsarrat!</p>

</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_24 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_10">

<div class="et_pb_column et_pb_column_1_2  et_pb_column_25">

	<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_1 et_always_center_on_mobile">
	<img src="<?php echo get_template_directory_uri(); ?>/images/Book-Cover.png" alt="" />

	</div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_26">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  et_pb_blurb_8 
et_pb_blurb_position_left">
	<div class="et_pb_blurb_content">
		<div class="et_pb_main_blurb_image"><span class="et-pb-icon et-waypoint et_pb_animation_left" style="color: 
#fbab09;">N</span></div>
		<div class="et_pb_blurb_container">
			<h4>Placeholder for Long Advantage 1</h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb --><div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  
et_pb_blurb_9 et_pb_blurb_position_left">
<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><span class="et-pb-icon et-waypoint et_pb_animation_left" style="color: 
#fbab09;">N</span></div>
	<div class="et_pb_blurb_container">
		<h4>Placeholder for Very Long Advantage</h4>

	</div>
</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb --><div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  
et_pb_blurb_10 et_pb_blurb_position_left">
<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><span class="et-pb-icon et-waypoint et_pb_animation_left" style="color: 
#fbab09;">N</span></div>
	<div class="et_pb_blurb_container">
		<h4>Advantage XS</h4>

	</div>
</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
<div id="et_pb_contact_form_0" class="et_pb_module et_pb_contact_form_container clearfix  et_pb_contact_form_0" data-
form_unique_num="0">

	<div class="et-pb-contact-message"></div>

	<div class="et_pb_contact">
		<form class="et_pb_contact_form clearfix" method="post" action="http://localhost/Monsarrat/?page_id=42">
			<p class="et_pb_contact_field et_pb_contact_field_0 et_pb_contact_field_last">
				<label for="et_pb_contact_name_1" class="et_pb_contact_form_label">Name</label>
				<input type="text" id="et_pb_contact_name_1" class="input" value="Name" 
name="et_pb_contact_name_1" data-required_mark="required" data-field_type="input" data-original_id="name">
			</p><p class="et_pb_contact_field et_pb_contact_field_1 et_pb_contact_field_last">
			<label for="et_pb_contact_email_1" class="et_pb_contact_form_label">Email Address</label>
			<input type="text" id="et_pb_contact_email_1" class="input" value="Email Address" 
name="et_pb_contact_email_1" data-required_mark="required" data-field_type="email" data-original_id="email">
		</p>
		<input type="hidden" value="et_contact_proccess" name="et_pb_contactform_submit_0">
		<input type="text" value="" name="et_pb_contactform_validate_0" class="et_pb_contactform_validate_field" />
		<div class="et_contact_bottom_container">

			<button type="submit" class="et_pb_contact_submit et_pb_button">Submit</button>
		</div>
		<input type="hidden" id="_wpnonce-et-pb-contact-form-submitted" name="_wpnonce-et-pb-contact-form-
submitted" value="5ab2e12e62" /><input type="hidden" name="_wp_http_referer" value="/Monsarrat/?page_id=42" />
	</form>
</div> <!-- .et_pb_contact -->
</div> <!-- .et_pb_contact_form_container -->
<div class="et_pb_code et_pb_module  et_pb_code_0">
	<a style="color: #4a4a4a;" href="#">By clicking “Get It” you agree our <strong style="color: #2b87da;">Terms & 
Service</strong></a>
</div> <!-- .et_pb_code --><div class="et_pb_module et_pb_accordion  et_pb_accordion_0">

</div> <!-- .et_pb_accordion --><div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  
et_pb_text_8">

</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section --><div class="et_pb_section  et_pb_section_5 et_section_regular">



<div class=" et_pb_row et_pb_row_11">

	<div class="et_pb_column et_pb_column_4_4  et_pb_column_27">

		<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_9">

			<p>From the creators</p>

		</div> <!-- .et_pb_text -->
	</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

<div class=" et_pb_row et_pb_row_12">
	<div class="et_pb_column et_pb_column_1_3  et_pb_column_28 et_pb_column_empty">
	</div> <!-- .et_pb_column -->
	<div class="et_pb_column et_pb_column_1_3  et_pb_column_29">
		<div class="et_pb_module et_pb_space et_pb_divider et_pb_divider_3"></div>
	</div> <!-- .et_pb_column -->

	<div class="et_pb_column et_pb_column_1_3  et_pb_column_30 et_pb_column_empty">
	</div> <!-- .et_pb_column -->
</div> <!-- .et_pb_row -->






<div class=" et_pb_row et_pb_row_12">
	<?php do_action('slideshow_deploy', '111'); ?>
</div> <!-- .et_pb_row -->


<div class=" et_pb_row et_pb_row_13">

<div class="et_pb_column et_pb_column_1_4  et_pb_column_31 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_32">

<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_10">

	<p>The new Gaming isn’t an alienating, separating break from life. It is life. It is posibly. It is friendship. It 
is the serendipity of millions of people, around the world, who want to engage.</p>

</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_33 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section --><div class="et_pb_section et_pb_fullwidth_section  et_pb_section_6 et_section_regular">



<div class="et_pb_module et_pb_slider  et_pb_fullwidth_slider_0">
	<div class="et_pb_slides">


	</div> <!-- .et_pb_slides -->
</div> <!-- .et_pb_slider -->


</div> <!-- .et_pb_section -->
<div class="et_pb_section  et_pb_section_7 et_section_regular">



<div class=" et_pb_row et_pb_row_14">

	<div class="et_pb_column et_pb_column_4_4  et_pb_column_34">

		<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_11">

			<p>Our news</p>

		</div> <!-- .et_pb_text -->
	</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_15">

<div class="et_pb_column et_pb_column_1_3  et_pb_column_35 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_36">

<div class="et_pb_module et_pb_space et_pb_divider et_pb_divider_4"></div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_37 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

<div class=" et_pb_row et_pb_row_16">

	<ul class="">
	<?php
		$args = array(
			'numberposts' => 3,
			'offset' => 0,
			'category' => 0,
			'orderby' => 'post_date',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' =>'',
			'post_type' => 'post',
			'post_status' => 'draft, publish, future, pending, private',
			'suppress_filters' => true
		);

		$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
		?>
		<?php foreach($recent_posts as $post) { ?>	

			<div class="et_pb_column et_pb_column_1_3 msr-news-item">
				<?php echo get_the_post_thumbnail($post['ID']); ?>	
				<div class="msr-news-item-wrap">
					
					<?php $categories = get_the_category( $post['ID']) ?>					
					<?php $date = substr($post['post_date'], 0,10) ?>
					<?php $date = explode('-',$date); ?>
					<?php $date = array_reverse($date); ?>
					<?php $date = implode('.', $date) ?>

					<h5 class="msr-post-slug"><?php echo $date .' / '.$categories[0]->slug ?></h5>
					<h3 class="mrs-post-title"><?php echo $post['post_title'] ?></h3>
					
					<?php $short_content = substr($post['post_content'], 0, 127); ?>
					
					<p class="msr-post-content"><?php echo $short_content ?></p>
					<div class="msr-divice"></div>
					
					<span class="et-pb-icon et-waypoint et_pb_animation_left et-animated msr-et-pb-
icon" style="color: #fbab09;">5</span>
					<a class="msr-readmore" href="<?php echo $post['guid'] ?>">Read Post </a>
				</div>
			</div>
		<?php } ?>
	</ul>
</div> <!-- .et_pb_row -->

<div class=" et_pb_row et_pb_row_17">

	<div class="et_pb_column et_pb_column_1_3  et_pb_column_41 et_pb_column_empty">


	</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_42">

	<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center ">
		<a href="#" type="button" class="mrs-see-all center-block text-center">SEE ALL</a>
	</div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_43 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div>

<div class=" et_pb_row et_pb_row_16">

<div class="et_pb_column et_pb_column_4_4  et_pb_column_38 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section -->



</div> <!-- .entry-content -->


</article> <!-- .et_pb_post -->



</div> <!-- #main-content -->


<?php 
get_footer();
?>