<div id="main-content">



	<article id="post-42" class="post-42 page type-page status-publish hentry">


		<div class="entry-content">
			<div class="et_pb_section et_pb_fullwidth_section  et_pb_section_0 et_section_regular ">
				
			<?php 
				if(!wp_is_mobile()){
					/*if ( function_exists( 'soliloquy' ) ) { soliloquy( '133' ); } */?>
					<div class='et_pb_section_0-background'>
					<!-- <img src="<?php echo get_template_directory_uri(); ?>/images/backhome-min.png" /> -->
						<div class='play-button'>
							<!-- <h2>Big Movement Gaming <br>makes life better.</h2> -->
							<h2>Big Movement Gaming makes life better</h2>
						<img src="<?php echo get_template_directory_uri(); ?>/images/Group.png" />
						</div>
						<div class='video-modal'><div class='close-modal-video'></div>
							<iframe class='video-frame' src="https://player.vimeo.com/video/218201194" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
						</div>
					</div>
					<!-- 	<div class='video-modal-wr'>

						</div> -->
				<?php } else{ ?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/Hero-6.jpg" />
					<!-- <ul class="bxslider"> -->
					<!-- <ul class="">
					  <li></li>
					  <li><img src="<?php echo get_template_directory_uri(); ?>/images/Hero-6.jpg" /></li>
					</ul> -->
				<?php } 
			?>	

			</div> <!-- .et_pb_section --><div class="et_pb_section  et_pb_section_1 et_section_regular">
<style>
/*.video-modal{
	display: none;
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    text-align: center;
    width: 100%;
    height: 100%;
    border: 3px solid #777;
    margin: 0 auto;
    padding: 5% 10%;
    background: rgba(0,0,0,0.4);
    z-index: 99999;
}
.video-modal-wr{
	position: relative;
	background: #eee;
    padding: 4px;
    padding-top: 25px;
    max-width: 960px;
    margin: 0 auto;
}*/
.play-button img, .close-modal-video{
	cursor: pointer;
}
.close-modal-video{
	width: 15px;
	height: 15px;
	position: absolute;
	top: 6px;
	right: 5px;
	background:url(<?php echo get_template_directory_uri(); ?>/images/icon-close.png);
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
}
.et_pb_section_0-background{
	padding-top: 32px;
	width: 100%;
    background-image: url(<?php echo get_template_directory_uri(); ?>/images/backhome-min-mn1.jpg);
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
}
	.play-button{
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
    margin: 0 auto;
    left: 0;
    right: 0;
    text-align: center;
z-index: 2;
padding: 14% 0;
	}

.play-button h2{
	margin-bottom: 15px;
	font-size: 42px;
    color: #fff;
    font-weight: 700;
    font-family: Raleway;
}
.video-frame{
	visibility: hidden;
}
.video-frame.visible{
	visibility: visible;
	z-index: 3;
}

@media(max-width: 1100px){
	.et_pb_section_0-background{
		/*height: 500px;*/
	}
	.play-button{
		/*top:25%;*/
	}
}
@media(max-width: 767px){
	.et_pb_section_0-background{
		/*height: 400px;*/
	}
	.play-button{
		/*top:25%;*/
	}

	.play-button h2{
	font-size: 32px;
	}
	.play-button img{
		width: 60px;
	}
	.play-button h2{
		/*margin-bottom: 18%;*/
	}
}
@media(max-width: 502px){
	.et_pb_section_0-background{
		/*height: 250px;*/
	}
	.play-button{
		/*top:25%;*/
	}
	.play-button h2{
	font-size: 21px;
/*	margin-bottom: 15px;*/
	}
	.play-button img{
		width: 40px;

	}
}
</style>
<script>
	jQuery('.play-button img').click(function(){
		jQuery('.video-frame').addClass('visible');
		var attr = jQuery('.video-modal iframe').attr('src');
			jQuery('.video-modal iframe').attr('src',attr+'&autoplay=1');
	});
	// jQuery('.close-modal-video, .video-modal').click(function(){
	// 	var src = jQuery('.video-modal iframe').attr('src');
	// 	jQuery('.video-modal iframe').attr('src',src);
	// 	jQuery('.video-modal').hide();
	// 	// jQuery('body').css({'overflow-y':'auto'});
	// });
</script>

			<div class=" et_pb_row et_pb_row_0">
				
				<div class="et_pb_column et_pb_column_1_2  et_pb_column_0">

					<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_0 et_always_center_on_mobile">
					<img class="msr-img-forget-online-gaming"  src="<?php echo get_template_directory_uri(); ?>/images/Bitmap.jpg" alt="" />
					<h4 class="msr-real-screenshot">Real Screenshot</h4>

					</div>
				</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_1">
				
				<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_0">

					<p>This is Gaming Now <br>
<span style='font-size: 24px'>Physical. Social. Local. Global.</span></p>

				</div> <!-- .et_pb_text --><div class="et_pb_module et_pb_space et_pb_divider et_pb_divider_0 et-hide-mobile"></div><div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_1">
				
				<p>The new videogames don't separate you from life. They are life. They are millions of people who want to play live and in person in the real world.</p>

			</div> <!-- .et_pb_text -->

			<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_1">
				
				<p>In one month, Pokémon Go earned $268 million. In two months, they lost 80% of players. We add longevity to augmented reality gaming with our experience in MMO games (like World of Warcraft), a $20 billion annual market. First we're focused on a <b>mobile AR game</b>. Then we'll launch a <b>platform for AR experiences</b>.</p>

			</div> <!-- .et_pb_text -->

			<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_1">
				
				<p>You <b>move big</b> through the real world making friends. You join <b>big movements</b> of players fighting for global control.</p>

			</div> <!-- .et_pb_text -->

			<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_3">
				<p class="msr-big-movements"><i>This</i> is <a href="https://monsarrat.com/vision/">Big Movement Gaming.</a></p>
			</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section -->



<div style="background-color: #FBAB09; height: 6px; margin: 20px 20% 50px 20%;"></div>

<div class=" et_pb_row et_pb_row_11">

	<div class="et_pb_column et_pb_column_4_4  et_pb_column_27">

		<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_9">

			<p>From the World's Best Qualified Team<br>to Surpass Pokémon Go</p>

		</div> <!-- .et_pb_text -->
	</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

<div style="text-align: center; height: 350px; vertical-align: top;">
<div style="width: 300px; text-align: center; padding: 10px; display: inline-block;">
<img src="https://monsarrat.com/wp-content/uploads/2016/11/Use-This-500x500-S-300x300.jpg" style="height: 150px;"><br>
<b>Johnny Monsarrat</b><br>
Founder/CEO of Turbine, a $160 million Warner Bros. acquisition. Invented MMO gaming, now a $20 billion industry.<br>
<a href="http://linkedin.com/in/monsarrat" target="_blank"><img src="https://monsarrat.com/wp-content/uploads/2016/12/linkedin-1.png"></a>
</div>

<div style="width: 300px; text-align: center; padding: 10px; display: inline-block;">
<img src="https://monsarrat.com/wp-content/uploads/2016/12/Brian-Sullivan.jpg" style="height: 150px;"><br>
<b>Brian Sullivan</b><br>
Co-creator of Age of Empires, Titan Quest, whose games have sold 25 million copies, earning $1 billion worldwide.<br>
<a href="https://www.linkedin.com/in/brian-sullivan-8ab700/" target="_blank"><img src="https://monsarrat.com/wp-content/uploads/2016/12/linkedin-1.png"></a>
</div>

<div style="width: 300px; text-align: center; padding: 10px; display: inline-block;">
<img src="https://monsarrat.com/wp-content/uploads/2016/12/Zoo-Anonymous.jpg" style="height: 150px;"><br>
<b>Dozens of Big Movers</b><br>
Whose <a href="https://monsarrat.com/team/">previous MMO and mobile games</a> include Age of Empires, Asheron's Call, Dungeons &amp; Dragons Online, Lord of the Rings Online, Game of Thrones: Conquest, and Titan Quest.
</div>
</div>

<div style="clear:both;"></div>


<div class="et_pb_section  et_pb_section_2 et_pb_with_background et_section_regular">

<div class=" et_pb_row et_pb_row_1">

	<div class="et_pb_column et_pb_column_4_4  et_pb_column_2">

		<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_text_4">

			<p>Play for Years</p>

		</div> <!-- .et_pb_text -->
	</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_2">

<div class="et_pb_column et_pb_column_1_3  et_pb_column_3 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_4">

<div class="et_pb_module et_pb_space et_pb_divider et_pb_divider_1"></div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_5 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_3">

<div class="et_pb_column et_pb_column_1_4  et_pb_column_6 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_7">

<div class="et_pb_text et_pb_module et_pb_bg_layout_dark et_pb_text_align_center  et_pb_text_5">

	<p>Pokémon Go flew high but crashed quickly. Big movement games last longer, combining the longevity of Massively Multiplayer Online Games with the real world, social, exploration play of Augmented Reality.</p>

</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_8 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_4">

<div class="et_pb_column et_pb_column_4_4  et_pb_column_9">

	<div class="et_pb_module et_pb_video  et_pb_video_0">
		<div class="et_pb_video_box">
			<iframe src="https://player.vimeo.com/video/193714904" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</div>
		<div class="et_pb_video_overlay" style="background-image: url(/wp-content/uploads/2016/12/testimonials-boston-common.jpg);">
			<div class="et_pb_video_overlay_hover">
				<a href="#" class="et_pb_video_play"></a>
			</div>
		</div>
	</div>
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section --><div class="et_pb_section  et_pb_section_3 et_section_regular">



<div class=" et_pb_row et_pb_row_5">

	<div class="et_pb_column et_pb_column_1_4  et_pb_column_10">

		<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_0 et_pb_blurb_position_top">
			<div class="et_pb_blurb_content">
			<div class="et_pb_main_blurb_image"><a href="https://monsarrat.com/vision/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Seek-It.png" alt="seek it" class="et-waypoint et_pb_animation_top" /></a></div>
				<div class="et_pb_blurb_container">
					<h4><a href="https://monsarrat.com/vision/" target="_blank">Seek it</a></h4>


				</div>
			</div> <!-- .et_pb_blurb_content -->
		</div> <!-- .et_pb_blurb -->
	</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_11">

	<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_1 et_pb_blurb_position_top">
		<div class="et_pb_blurb_content">
		<div class="et_pb_main_blurb_image"><a href="https://monsarrat.com/vision/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Make-it.png" alt="make it" class="et-waypoint et_pb_animation_top" /></a></div>
			<div class="et_pb_blurb_container">
				<h4><a href="https://monsarrat.com/vision/" target="_blank">Tweak it</a></h4>

			</div>
		</div> <!-- .et_pb_blurb_content -->
	</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_12">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_2 et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="https://monsarrat.com/vision/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Join-It.png" alt="Join it" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="https://monsarrat.com/vision/" target="_blank">Join it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_13">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_3 et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="https://monsarrat.com/vision/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Greet.png" alt="" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="https://monsarrat.com/vision/" target="_blank">Greet it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_6">

<div class="et_pb_column et_pb_column_1_4  et_pb_column_14">

	<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_4 et_pb_blurb_position_top">
		<div class="et_pb_blurb_content">
		<div class="et_pb_main_blurb_image"><a href="https://monsarrat.com/vision/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Page-1.png" alt="Dare it" class="et-waypoint et_pb_animation_top" /></a></div>
			<div class="et_pb_blurb_container">
				<h4><a href="https://monsarrat.com/vision/" target="_blank">Dare it</a></h4>


			</div>
		</div> <!-- .et_pb_blurb_content -->
	</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_15">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_5 et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="https://monsarrat.com/vision/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Share-It.png" alt="Share it" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="https://monsarrat.com/vision/" target="_blank">Share it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_16">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_6 et_pb_blurb_position_top">
	

	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="https://monsarrat.com/vision/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Fight-It.png" alt="Fight it" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="https://monsarrat.com/vision/" target="_blank">Fight it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->

	
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_17">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_blurb_7 et_pb_blurb_position_top">
	<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image"><a href="https://monsarrat.com/vision/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/Beat.png" alt="Beat it" class="et-waypoint et_pb_animation_top" /></a></div>
		<div class="et_pb_blurb_container">
			<h4><a href="https://monsarrat.com/vision/" target="_blank">Beat it</a></h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section --><div class="et_pb_section  et_pb_section_4 et_pb_with_background et_section_regular">



<div class=" et_pb_row et_pb_row_7">

	<div class="et_pb_column et_pb_column_4_4  et_pb_column_18">

		<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_6">

			<p>See the Game!</p>

		</div> <!-- .et_pb_text -->
	</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_8">

<div class="et_pb_column et_pb_column_1_3  et_pb_column_19 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_20">

<div class="et_pb_module et_pb_space et_pb_divider et_pb_divider_2"></div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_21 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_9">

<div class="et_pb_column et_pb_column_1_4  et_pb_column_22 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_23">

<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_7">

	<p>Get the free video instantly by signing up for news.</p>

</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_4  et_pb_column_24 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_10">

<div class="et_pb_column et_pb_column_1_2  et_pb_column_25">
<div class="et_pb_video_overlay_hover" style="">
				<a data-toggle="modal" data-target="#myModal" href="#" class="et_pb_video_play" style="color: #fbab09; top: 45%;"></a>
			</div>
	<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_1 et_always_center_on_mobile">
	<a data-toggle="modal" data-target="#myModal" href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/Book-Cover.jpg" alt="" /></a>

	</div>
	<div id="myModal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <!-- <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Modal Header</h4>
	      </div> -->
	      <div class="modal-body">
	        <p>To see this video, sign up for free news and sneak peeks!</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_2  et_pb_column_26">

<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  et_pb_blurb_8 et_pb_blurb_position_left">
	<div class="et_pb_blurb_content">
		<div class="et_pb_main_blurb_image img-second"><span class="et-pb-icon et-waypoint et_pb_animation_left" style="color: #fbab09;">N</span></div>
		<div class="et_pb_blurb_container">
			<h4>Get Sneak Peeks</h4>

		</div>
	</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb --><div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  et_pb_blurb_9 et_pb_blurb_position_left">
<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image img-second"><span class="et-pb-icon et-waypoint et_pb_animation_left" style="color: #fbab09;">N</span></div>
	<div class="et_pb_blurb_container">
		<h4>Give Feedback</h4>

	</div>
</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb --><div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  et_pb_blurb_10 et_pb_blurb_position_left">
<div class="et_pb_blurb_content">
	<div class="et_pb_main_blurb_image img-second"><span class="et-pb-icon et-waypoint et_pb_animation_left" style="color: #fbab09;">N</span></div>
	<div class="et_pb_blurb_container">
		<h4>See the Future</h4>

	</div>
</div> <!-- .et_pb_blurb_content -->
</div> <!-- .et_pb_blurb -->
<div id="et_pb_contact_form_0" class="et_pb_module et_pb_contact_form_container clearfix  et_pb_contact_form_0" data-form_unique_num="0">

	<div class="et-pb-contact-message"></div>

	<div class="et_pb_contact">
		
		<!-- Begin MailChimp Signup Form -->
		<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
		<!-- <style type="text/css">
			#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
			/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
			   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
		</style> -->
		<div id="mc_embed_signup">
		<!-- <form action="//coffeetshirt.us14.list-manage.com/subscribe/post?u=6e64cf168896af6b04d70f21c&amp;id=8bfd83dbb6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate> -->
		 		<form action="//monsarrat.us14.list-manage.com/subscribe/post?u=2bb50591660f477ce99802cb4&id=50736ea731" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
		    <div id="mc_embed_signup_scroll">
			
                <div class="mc-field-group">
                        
                        <input type="text" placeholder="First Name" value="" name="FNAME" class="es_txt_name " id="mce-FNAME">
                </div>
		<div class="mc-field-group">
			<input placeholder="Email" type="email" value="" name="EMAIL" class="required email es_txt_email" id="mce-EMAIL">
		</div>
			
		    
		    <div class="clear"><input type="submit" value="Get It" name="subscribe" id="mc-embedded-subscribe" class="button es_txt_button"></div>
		    <div id="mce-responses" class="clear">
				<div class="response" id="mce-error-response" style="display:none"></div>
				<div class="response" id="mce-success-response" style="display:none"></div>
			</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
		    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6e64cf168896af6b04d70f21c_8bfd83dbb6" tabindex="-1" value=""></div>
		    </div>
		</form>
		</div>
		<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
		<!--End mc_embed_signup-->

</div> <!-- .et_pb_contact -->
</div> <!-- .et_pb_contact_form_container -->
<div class="et_pb_code et_pb_module  et_pb_code_0">
	<!-- <a style="color: #4a4a4a;" href="#">By clicking “Get It” you agree our <strong style="color: #2b87da;">Terms & Service</strong></a> -->
</div> <!-- .et_pb_code --><div class="et_pb_module et_pb_accordion  et_pb_accordion_0">

</div> <!-- .et_pb_accordion --><div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_8">

</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section --><div class="et_pb_section et_pb_fullwidth_section  et_pb_section_6 et_section_regular">



</div> <!-- .et_pb_section -->
<div class="et_pb_section  et_pb_section_7 et_section_regular">



<div class=" et_pb_row et_pb_row_14">

	<div class="et_pb_column et_pb_column_4_4  et_pb_column_34">

		<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_center  et_pb_text_11">

			<p>News</p>

		</div> <!-- .et_pb_text -->
	</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row --><div class=" et_pb_row et_pb_row_15">

<div class="et_pb_column et_pb_column_1_3  et_pb_column_35 et_pb_column_empty">


</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_36">

<div class="et_pb_module et_pb_space et_pb_divider et_pb_divider_4"></div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_37 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

<div class=" et_pb_row et_pb_row_16">

	<ul class="">
	<?php
		$args = array(
			'numberposts' => 3,
			'offset' => 0,
			'category' => 0,
			'orderby' => 'post_date',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' =>'',
			'post_type' => 'post',
			'post_status' => 'draft, publish, future, pending, private',
			'suppress_filters' => true
		);

		$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
		?>
		<?php foreach($recent_posts as $post) { ?>	

			<div class="et_pb_column et_pb_column_1_3 msr-news-item" style="min-height: 423px;">
				<?php echo get_the_post_thumbnail($post['ID']); ?>	
				<div class="msr-news-item-wrap">
					
					<?php $categories = get_the_category( $post['ID']) ?>					
					<?php $date = substr($post['post_date'], 0,10) ?>
					<?php $date = explode('-',$date); ?>
					<?php $date = array_reverse($date); ?>
					<?php $date = implode('.', $date) ?>

<?php $date = date('m/d/Y', strtotime($date)) ?>

					<h5 class="msr-post-slug"><?php echo $date ?></h5>
					
<a href="<?php echo get_permalink($post['ID']); ?>">					
<h3 class="mrs-post-title"><?php echo  $post['post_title'] ?></h3>
</a>
					
					<?php $short_content = substr($post['post_content'], 0, 127); ?>
					
					<p class="msr-post-content"><?php echo $short_content ?></p>
					<div class="msr-divice"></div>
					
					<span class="et-pb-icon et-waypoint et_pb_animation_left et-animated msr-et-pb-icon" style="color: #fbab09;">5</span>
					<a class="msr-readmore" href="<?php echo get_permalink($post['ID']); ?>">Read Post </a>
				</div>
			</div>
		<?php } ?>
	</ul>
</div> <!-- .et_pb_row -->

<div class=" et_pb_row et_pb_row_17">

	<div class="et_pb_column et_pb_column_1_3  et_pb_column_41 et_pb_column_empty">


	</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_42">

	<div class="et_pb_button_module_wrapper et_pb_module et_pb_button_alignment_center ">
		<a href="<?php echo get_permalink(160); ?>" type="button" class="mrs-see-all center-block text-center">SEE ALL</a>
	</div>
</div> <!-- .et_pb_column --><div class="et_pb_column et_pb_column_1_3  et_pb_column_43 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div>

<div class=" et_pb_row et_pb_row_16">

<div class="et_pb_column et_pb_column_4_4  et_pb_column_38 et_pb_column_empty">


</div> <!-- .et_pb_column -->

</div> <!-- .et_pb_row -->

</div> <!-- .et_pb_section -->



</div> <!-- .entry-content -->


</article> <!-- .et_pb_post -->



</div> <!-- #main-content -->
