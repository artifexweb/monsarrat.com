<?php

/*
 * Template Name: News Page V2
*/


get_header();

$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );

?>

<div id="main-content" data-track="page-news">

<?php if ( ! $is_page_builder_used ) : ?>

	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area" class="mrs-left-area">
			<h1 class="entry-title main_title msr-post-title"><?php _e('News') ?></h1>
<?php endif; ?>

			<?php
	//	global $wp_query;

		$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
		$postsPerPage = 4;
	//	$postOffset = $paged * $postsPerPage;

		// $paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
		$args = array(
		//	'numberposts' => -1,
			'posts_per_page' => $postsPerPage,
			'paged' => $paged,
		//	'offset' => $postOffset,
			// 'category' => 4,
			'orderby' => 'post_date',
			'order' => 'DESC',
			'include' => '',
			'exclude' => '',
			'meta_key' => '',
			'meta_value' =>'',
			'post_type' => 'post',
		//	'post_status' => 'draft, publish, future, pending, private',
			'post_status' => 'publish',
			'suppress_filters' => true
		);

		$wp_query = new WP_Query( $args );

		?>
		<?php 
			if ( $wp_query->have_posts() ) : while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>
			<h4 class="mentry-title main_title-news msr-post-title-news">
				<a href="<?php echo get_permalink($post->ID) ?>">
					<?php echo $post->post_title ?>
				</a>
			</h4>

			<?php $date = substr($post->post_date, 0,10) ?>
			<?php $date = explode('-',$date); ?>
			<?php $date = array_reverse($date); ?>
			<?php $date = implode('.', $date) ?>
			<?php $date = date('m.d.Y', strtotime($date)) ?>

			<h5 class="msr-post-slug-news"><?php echo $date;?></h5>

			<div class="et_pb_column et_pb_column_1_3  et_pb_column_2">
				<div class="et_pb_module et-waypoint et_pb_image et_pb_animation_left et_pb_image_0 et_always_center_on_mobile et-animated">
					<a href="<?php echo get_permalink($post->ID) ?>">
						<?php echo get_the_post_thumbnail($post->ID); ?>
					</a>
				</div>
			</div> <!-- .et_pb_column -->

			<div class="et_pb_column et_pb_column_1_2  et_pb_column_3">

				<div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_2">
					<?php $short_content = substr($post->post_content, 0, 150); ?>

					<p><?php echo $short_content.'...' ?></p>

				</div> <!-- .et_pb_text -->
				<span class="et-pb-icon et-waypoint et_pb_animation_left et-animated msr-et-pb-icon" style="color: #fbab09;">5</span>
				<a class="msr-readmore" href="<?php echo get_permalink($post->ID) ?>">Read Post </a>
			</div> <!-- .et_pb_column -->
			<div class="clearfix"></div>
			<div class="msr-divice-news"></div>

<?php 
endwhile; endif;

numeric_posts_nav();

wp_reset_query();
 ?>

<?php if ( ! $is_page_builder_used ) : ?>

			</div> <!-- #left-area -->

			<?php get_sidebar(); ?>
		</div> <!-- #content-area -->
	</div> <!-- .container -->

<?php endif; ?>

</div> <!-- #main-content -->

<?php get_footer(); ?>


