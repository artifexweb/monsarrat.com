<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php elegant_description(); ?>
	<?php elegant_keywords(); ?>
	<?php elegant_canonical(); ?>

	<?php do_action( 'et_head_meta' ); ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery-2.1.1.min.js"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<?php $template_directory_uri = get_template_directory_uri(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
	<![endif]-->

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<script type="text/javascript">
		jQuery('body').removeClass('et_fixed_nav');
	</script>
	<div id="page-container">
		<header id="main-header" data-height-onload="80" data-height-loaded="true" style="top: 32px; right: 0px; left: 0px; width: 100%;" class="">
			<div class="container clearfix et_menu_container">
				<div class="logo_container">
					<span class="logo_helper"></span>
					<a href="<?php echo home_url(); ?>">
						<img src="<?php echo get_template_directory_uri(); ?>/images/logo-216x54.png" alt="Monsarrat" id="logo" data-height-percentage="100" data-actual-width="216" data-actual-height="54">
					</a>
				</div>
				<div id="et-top-navigation" data-height="66" data-fixed-height="40">
					<nav id="top-menu-nav">
						<ul id="top-menu" class="nav">

							<?php
	                            /**
	                             * Displays a navigation menu
	                             * @param array $args Arguments
	                             */
	                            $args = array(
	                            	'theme_location' => '',
	                            	'menu' => 'Page menu',
	                            	'menu_class' => 'nav navbar-nav navbar-right',
	                            	'menu_id' => '',
	                            	'echo' => true,
	                            	'fallback_cb' => 'wp_page_menu',
	                            	'before' => '',
	                            	'after' => '',
	                            	'link_before' => '',
	                            	'link_after' => '',
	                            	'items_wrap' => '<ul id = "%1$s" class = "%2$s">%3$s</ul>',
	                            	'depth' => 0,
	                            	'walker' => ''
	                            	);

	                            wp_nav_menu($args);

	                            ?>
	                        </ul>
	                    </nav>
	                    <div id="et_mobile_nav_menu">
	                    	<div class="mobile_nav closed">
	                    		<span class="select_page">Select Page</span>
	                    		<span class="mobile_menu_bar mobile_menu_bar_toggle"></span>
	                    		<ul id="mobile_menu" class="et_mobile_menu">

	                    			<?php
		                            /**
		                             * Displays a navigation menu
		                             * @param array $args Arguments
		                             */
		                           
		                            wp_nav_menu($args);

		                            ?>
		                        </ul>
		                    </div>
		                </div>				
		            </div> <!-- #et-top-navigation -->
		        </div> <!-- .container -->
		    </header>
		    <div id="et-main-area">