<?php
if ( ( is_single() || is_page() ) && 'et_full_width_page' === get_post_meta( get_queried_object_id(), '_et_pb_page_layout', true ) )
	return;

if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
<div id="sidebar">

	<div><h3 class="getforfree">See the Game!</h3></div>

	<div class="">

		<div class="et_pb_column et_pb_column_4_4" style="margin-bottom: 20px; ">

<div id="myModal" class="modal fade" role="dialog" style="display: none; top: 24%;">
	  <div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
	      <!-- <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title">Modal Header</h4>
	      </div> -->
	      <div class="modal-body">
	        <p>To see this video, sign up for free news and sneak peeks!</p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>

			<div class="et_pb_module et-waypoint et_pb_image  et_pb_image_1 et_always_center_on_mobile">
				<img src="<?php echo get_template_directory_uri(); ?>/images/Book-Cover.jpg" alt="" />

			</div>

			<div data-toggle="modal" data-target="#myModal" class="et_pb_video_overlay msr-sidebar-video" style="background-image: url(<?php echo get_template_directory_uri(); ?>/images/Book-Cover.jpg);">
				<div class="et_pb_video_overlay_hover">
					<a href="#" class="et_pb_video_play msr-play-button"></a> 
				</div>


				
			</div>


		</div> <!-- .et_pb_column -->
		<div > <h4 class="msr-abcxxx">Sign up now to get the video.</h4></div>
		<div style="margin-bottom: 20px;">
			<div class="msr-check-text-contain">
				<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  et_pb_blurb_8 et_pb_blurb_position_left ">
					<div class="et_pb_blurb_content">
						<div class="et_pb_main_blurb_image img-second"><span class="et-pb-icon et-waypoint et_pb_animation_left msr-check-img-sidebar" style="color: #fbab09;">N</span></div>
						<div class="et_pb_blurb_container msr-text-sidebar-video">
							<h4>Get Sneak Peeks</h4>

						</div>
					</div> <!-- .et_pb_blurb_content -->
				</div>

			</div>

			<div class="msr-check-text-contain-2">
				<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  et_pb_blurb_8 et_pb_blurb_position_left ">
					<div class="et_pb_blurb_content">
						<div class="et_pb_main_blurb_image img-second"><span class="et-pb-icon et-waypoint et_pb_animation_left msr-check-img-sidebar" style="color: #fbab09;">N</span></div>
						<div class="et_pb_blurb_container msr-text-sidebar-video">
							<h4>Give Feedback</h4>

						</div>
					</div> <!-- .et_pb_blurb_content -->
				</div>

			</div>

			<div class="msr-check-text-contain">
				<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  et_pb_blurb_8 et_pb_blurb_position_left ">
					<div class="et_pb_blurb_content">
						<div class="et_pb_main_blurb_image img-second"><span class="et-pb-icon et-waypoint et_pb_animation_left msr-check-img-sidebar" style="color: #fbab09;">N</span></div>
						<div class="et_pb_blurb_container msr-text-sidebar-video">
							<h4>See the Future</h4>

						</div>
					</div> <!-- .et_pb_blurb_content -->
				</div>

			</div>

			<div class="msr-check-text-contain-2">
				<div class="et_pb_blurb et_pb_module et_pb_bg_layout_light et_pb_text_align_right  et_pb_blurb_8 et_pb_blurb_position_left ">
					<div class="et_pb_blurb_content">
						<div class="et_pb_main_blurb_image img-second"><span class="et-pb-icon et-waypoint et_pb_animation_left msr-check-img-sidebar" style="color: #fbab09;">N</span></div>
						<div class="et_pb_blurb_container msr-text-sidebar-video">
							<h4 class="">Lose Your Mind</h4>

						</div>
					</div> <!-- .et_pb_blurb_content -->
				</div>

			</div>
		</div>

		
		
		
		<!-- <div class="msr-get-it-txt">

			If you have read this far, we both know you are serious about succeeding, getting the most out of life, and attaining your goals. Text more here!Now, how do you “keep the fire burning” during life’s daily.
		</div>  --><!-- .et_pb_column -->
		<div class="clearfix"></div>
		<div class="et_pb_column et_pb_column_4_4">




			<!-- Begin MailChimp Signup Form -->
			<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
			<!-- <style type="text/css">
				#mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
				/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
				   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
			</style> -->
			<div id="mc_embed_signup">
			<!-- <form action="//coffeetshirt.us14.list-manage.com/subscribe/post?u=6e64cf168896af6b04d70f21c&amp;id=8bfd83dbb6" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate> -->
			 		<form action="//monsarrat.us14.list-manage.com/subscribe/post?u=2bb50591660f477ce99802cb4&id=50736ea731" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
			    <div id="mc_embed_signup_scroll">
				
	                <div class="mc-field-group">
	                        
	                        <input type="text" placeholder="First Name" value="" name="FNAME" class="es_txt_name " id="mce-FNAME">
	                </div>
			<div class="mc-field-group">
				<input placeholder="Email" type="email" value="" name="EMAIL" class="required email es_txt_email" id="mce-EMAIL">
			</div>
				
			    
			    <div class="clear"><input type="submit" value="Get It" name="subscribe" id="mc-embedded-subscribe" class="button es_txt_button"></div>
			    <div id="mce-responses" class="clear">
					<div class="response" id="mce-error-response" style="display:none"></div>
					<div class="response" id="mce-success-response" style="display:none"></div>
				</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
			    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_6e64cf168896af6b04d70f21c_8bfd83dbb6" tabindex="-1" value=""></div>
			    </div>
			</form>
			</div>
			<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
			<!--End mc_embed_signup-->



		</div> <!-- .et_pb_column -->
		<div class="et_pb_contact">
				
			</div> <!-- .et_pb_contact -->
		<div id="et_pb_contact_form_0" class="et_pb_module et_pb_contact_form_container clearfix  et_pb_contact_form_0" data-form_unique_num="0">
			
	</div> <!-- .et_pb_contact_form_container -->
	<div class="et_pb_code et_pb_module  et_pb_code_0">
		<!-- <a style="color: #4a4a4a;" href="#">By clicking “Get It” you agree our <strong style="color: #2b87da;">Terms & Service</strong></a> -->
	</div> <!-- .et_pb_code --><div class="et_pb_module et_pb_accordion  et_pb_accordion_0">
</div> <!-- .et_pb_accordion --><div class="et_pb_text et_pb_module et_pb_bg_layout_light et_pb_text_align_left  et_pb_text_8">

</div> <!-- .et_pb_text -->
</div> <!-- .et_pb_column -->





<div>
	<div class="">
		<div><h3 class="getforfree fornews">News</h3></div>

		<ul class="">
			<?php
			$args = array(
				'numberposts' => 1,
				'offset' => 0,
				'category' => 0,
				'orderby' => 'post_date',
				'order' => 'DESC',
				'include' => '',
				'exclude' => '',
				'meta_key' => '',
				'meta_value' =>'',
				'post_type' => 'post',
				'post_status' => 'draft, publish, future, pending, private',
				'suppress_filters' => true
				);

			$recent_posts = wp_get_recent_posts( $args, ARRAY_A );
			?>
			<?php foreach($recent_posts as $post) { ?>	

			<div class="et_pb_column et_pb_column_4_4 msr-news-item-sidebar">
				<?php echo get_the_post_thumbnail($post['ID']); ?>	
				<div class="msr-news-item-wrap">

					<?php $categories = get_the_category( $post['ID']) ?>					
					<?php $date = substr($post['post_date'], 0,10) ?>
					<?php $date = explode('-',$date); ?>
					<?php $date = array_reverse($date); ?>
					<?php $date = implode('.', $date) ?>

					<h5 class="msr-post-slug"><?php echo $date.' / '.$categories[0]->slug ?></h5>
					<h3 class="mrs-post-title"><?php echo $post['post_title'] ?></h3>

					<?php $short_content = substr($post['post_content'], 0, 127); ?>

					<p class="msr-post-content"><?php echo $short_content ?></p>
					<div class="msr-divice"></div>

					<span class="et-pb-icon et-waypoint et_pb_animation_left et-animated msr-et-pb-icon" style="color: #fbab09;">5</span>
					<a class="msr-readmore" href="<?php echo $post['guid'] ?>">Read Post </a>
				</div>
			</div>
			<?php } ?>
		</ul>
		<div class="" style="width: 250px;">
			<a href="<?php echo get_permalink(160); ?>" type="button" class="msr-see-all">SEE ALL</a>
		</div>
	</div> <!-- .et_pb_row -->
</div>
</div> <!-- end #sidebar -->
<?php endif; ?>
